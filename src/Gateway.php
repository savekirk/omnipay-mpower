<?php

namespace Omnipay\Mpower;


use Omnipay\Common\AbstractGateway;


/**
 * @method \Omnipay\Common\Message\ResponseInterface authorize(array $options = array())
 * @method \Omnipay\Common\Message\ResponseInterface completeAuthorize(array $options = array())
 * @method \Omnipay\Common\Message\ResponseInterface capture(array $options = array())
 * @method \Omnipay\Common\Message\ResponseInterface refund(array $options = array())
 * @method \Omnipay\Common\Message\ResponseInterface void(array $options = array())
 * @method \Omnipay\Common\Message\ResponseInterface createCard(array $options = array())
 * @method \Omnipay\Common\Message\ResponseInterface updateCard(array $options = array())
 * @method \Omnipay\Common\Message\ResponseInterface deleteCard(array $options = array())
 */
class Gateway extends AbstractGateway
{
    private $masterKey = "masterKey";
    private $privateKey = "privateKey";
    private $publicKey = "publicKey";
    private $token = "token";


    /**
     * Get gateway display name
     *
     * This can be used by carts to get the display name for each gateway.
     */
    public function getName()
    {
        return "MPower Payments";
    }

    public function getDefaultParameters()
    {
        return array(
            $this->masterKey => '',
            $this->privateKey => '',
            $this->publicKey => '',
            $this->token => '',
            'testMode' => true
        );
    }

    public function setMasterKey($masterKey)
    {
        return $this->setParameter($this->masterKey, $masterKey);
    }

    public function setPrivateKey($privateKey)
    {
        return $this->setParameter($this->privateKey, $privateKey);
    }

    /**
     * @return string
     */
    public function getPrivateKey()
    {
        return $this->getParameter($this->privateKey);
    }


    /**
     * @return string
     */
    public function getPublicKey()
    {
        return $this->getParameter($this->publicKey);
    }

    /**
     * @param string $publicKey
     */
    public function setPublicKey($publicKey)
    {
        return $this->setParameter($this->publicKey, $publicKey);
    }

    /**
     * @return string
     */
    public function getMasterKey()
    {
        return $this->getParameter($this->masterKey);
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->getParameter($this->token);
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        return $this->setParameter($this->token, $token);
    }

    public function purchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Mpower\Message\PurchaseRequest', $parameters);
    }

    public function completePurchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Mpower\Message\CompletePurchaseRequest', $parameters);
    }




}