<?php


namespace Omnipay\Mpower\Message;


use Omnipay\Common\Message\AbstractResponse;
use Omnipay\Common\Message\RedirectResponseInterface;

class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{


    /**
     * Gets the redirect target url.
     *
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->isSuccessful() ? $this->data->response_text : null;
    }

    /**
     * Get the required redirect method (either GET or POST).
     *
     * @return string
     */
    public function getRedirectMethod()
    {
        return 'GET';
    }

    /**
     * Gets the redirect form data array, if the redirect method is POST.
     *
     * @return array
     */
    public function getRedirectData()
    {
        return '';
    }

    /**
     * Is the response successful?
     *
     * @return boolean
     */
    public function isSuccessful()
    {
        return  $this->getCode() === '00';
    }

    public function isRedirect()
    {
        return $this->isSuccessful();
    }

    public function getTransactionReference()
    {
        return isset($this->data->token) ? $this->data->token : null;
    }

    public function getMessage()
    {
        return $this->isSuccessful() ? $this->data->description : $this->data->response_text;
    }

    public function getCode()
    {
        return $this->data->response_code;
    }

    public function getToken()
    {
        return $this->data->token;
    }

}