<?php


namespace Omnipay\Mpower\Message;


class PurchaseRequest extends AbstractRequest
{
    public function getData()
    {
        $data = parent::getData();

        $invoice = array();
        $invoice['total_amount'] = $this->getAmount();
        $invoice['description'] = $this->getDescription();
        $data['invoice'] = $invoice;

        $actions = array();
        $actions['cancel_url'] = $this->getCancelUrl();
        $actions['return_url'] = $this->getReturnUrl();
        $data['actions'] = $actions;

        $custom_data = array();
        $custom_data['clientip'] = $this->getClientIp();
        $data['custom_data'] = $custom_data;


        return $data;

    }



}