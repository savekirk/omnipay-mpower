<?php


namespace Omnipay\Mpower\Message;


use Omnipay\Common\Message\ResponseInterface;

abstract class AbstractRequest extends \Omnipay\Common\Message\AbstractRequest
{

    protected $testEndpoint = 'https://app.mpowerpayments.com/sandbox-api/v1/checkout-invoice/create';
    protected $liveEndpoint = 'https://app.mpowerpayments.com/api/v1/checkout-invoice/create';

    private $masterKey = "masterKey";
    private $privateKey = "privateKey";
    private $publicKey = "publicKey";
    private $token = "token";


    public function setMasterKey($masterKey)
    {
        return $this->setParameter($this->masterKey, $masterKey);
    }

    public function setPrivateKey($privateKey)
    {
        return $this->setParameter($this->privateKey, $privateKey);
    }

    /**
     * @return string
     */
    public function getPrivateKey()
    {
        return $this->getParameter($this->privateKey);
    }


    /**
     * @return string
     */
    public function getPublicKey()
    {
        return $this->getParameter($this->publicKey);
    }

    /**
     * @param string $publicKey
     */
    public function setPublicKey($publicKey)
    {
        return $this->setParameter($this->publicKey, $publicKey);
    }

    /**
     * @return string
     */
    public function getMasterKey()
    {
        return $this->getParameter($this->masterKey);
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->getParameter($this->token);
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        return $this->setParameter($this->token, $token);
    }

    public function getHttMethod()
    {
        return "GET";
    }

    public function getEndpoint()
    {
        if ($this->getTestMode()) {
            return $this->testEndpoint;
        }
        return $this->liveEndpoint;
    }

    /**
     * Get the raw data array for this message. The format of this varies from gateway to
     * gateway, but will usually be either an associative array, or a SimpleXMLElement.
     *
     * @return mixed
     */
    public function getData()
    {
        $data = array();
        $data['store'] = array("name" => "Yusic");

        return $data;
    }

    /**
     * Send the request with specified data
     *
     * @param  mixed $data The data to send
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $headers = array(
            "MP-Master-Key" => $this->getMasterKey(),
            "MP-Private-Key" => $this->getPrivateKey(),
            "MP-Token" => $this->getToken()
        );


        $httpResponse = $this->httpClient->post($this->getEndpoint(), $headers, json_encode($data))->send();

        $response_data = json_decode($httpResponse->getBody());

        return $this->response = new PurchaseResponse($this, $response_data);
    }


}